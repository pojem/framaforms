# French translation of pgbar (7.x-1.5)
# Copyright (c) 2014 by the French translation team
#
msgid ""
msgstr ""
"Project-Id-Version: pgbar (7.x-1.5)\n"
"POT-Creation-Date: 2014-09-28 09:50+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n>1);\n"

msgid "Display"
msgstr "Affichage"
msgid "Style"
msgstr "Style"
msgid "Form key"
msgstr "Clé du formulaire"
msgid "Data source"
msgstr "Source de données"
msgid "Progress bar"
msgstr "Barre de progression"
msgid "Status message"
msgstr "Message d'état"
